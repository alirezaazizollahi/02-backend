package ir.paizan.ecommerce.services.impl;

import ir.paizan.ecommerce.entity.Product;
import ir.paizan.ecommerce.repository.ProductRepository;
import ir.paizan.ecommerce.services.ProductService;
import ir.paizan.ecommerce.services.dto.ProductDTO;
import ir.paizan.ecommerce.services.dtoMapper.ProductMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private ProductMapper productMapper;

    @Override
    public Page<ProductDTO> findAll(Pageable pageable) {
        return productRepository.findAll(pageable)
                .map(product -> productMapper.fromProductToProductDTO(product));
    }

    @Override
    public Product findById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        return product.isPresent() ? product.get() : null;
    }

    @Override
    public List<Product> findByCategoryId(Long categoryId) {
        List<Product> productList = productRepository.findByCategoryId(categoryId);
        return productList;
    }

    @Override
    public  Page<ProductDTO> findByCategoryId(Long categoryId, Pageable pageable) {
        return productRepository.findByCategoryId(categoryId, pageable)
                                .map(product -> productMapper.fromProductToProductDTO(product));
    }

    @Override
    public List<Product> findByName(String name) {
        List<Product> productList = productRepository.findByNameContaining(name);
        return productList;
    }

    @Override
    public Page<ProductDTO> findByName(String name, Pageable pageable) {
        return productRepository.findByNameContaining(name, pageable)
                .map(product -> productMapper.fromProductToProductDTO(product));
    }


}
