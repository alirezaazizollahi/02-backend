package ir.paizan.ecommerce.services;

import ir.paizan.ecommerce.entity.Product;
import ir.paizan.ecommerce.services.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {

    Page<ProductDTO> findAll(Pageable pageable);

    Product findById(Long id);

    List<Product> findByCategoryId(Long categoryId);

    Page<ProductDTO> findByCategoryId(Long categoryId, Pageable pageable);

    List<Product> findByName(String name);

    Page<ProductDTO> findByName(String name, Pageable pageable);
}
