package ir.paizan.ecommerce.services.dto;

import lombok.Data;

@Data
public class PurchaseResponse {

    private final String orderTrackingNumber;
}
