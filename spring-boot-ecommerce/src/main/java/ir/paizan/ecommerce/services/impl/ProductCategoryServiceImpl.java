package ir.paizan.ecommerce.services.impl;

import ir.paizan.ecommerce.entity.ProductCategory;
import ir.paizan.ecommerce.repository.ProductCategoryRepository;
import ir.paizan.ecommerce.services.ProductCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private ProductCategoryRepository productCategoryRepository;

    @Override
    public List<ProductCategory> findAll() {
        return productCategoryRepository.findAll();
    }

    @Override
    public ProductCategory findById(Long id) {
        Optional<ProductCategory> opt = productCategoryRepository.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }
}
