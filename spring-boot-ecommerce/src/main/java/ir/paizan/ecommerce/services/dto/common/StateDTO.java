package ir.paizan.ecommerce.services.dto.common;

import lombok.Data;

@Data
public class StateDTO {

    private Long id;
    private String name;

    private CountryDTO country;

}
