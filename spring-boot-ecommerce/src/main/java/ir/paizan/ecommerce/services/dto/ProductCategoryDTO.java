package ir.paizan.ecommerce.services.dto;

import lombok.Data;

@Data
public class ProductCategoryDTO {

    private Long id;
    private String categoryName;
}
