package ir.paizan.ecommerce.services;

import ir.paizan.ecommerce.entity.ProductCategory;

import java.util.List;

public interface ProductCategoryService {

    List<ProductCategory> findAll();

    ProductCategory findById(Long id);
}
