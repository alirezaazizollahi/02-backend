package ir.paizan.ecommerce.services.dto;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class CustomerDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Set<OrderDTO> orders = new HashSet<>();
}
