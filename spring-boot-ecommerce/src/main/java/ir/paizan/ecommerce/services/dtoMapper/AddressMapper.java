package ir.paizan.ecommerce.services.dtoMapper;

import ir.paizan.ecommerce.entity.Address;
import ir.paizan.ecommerce.services.dto.AddressDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    AddressDTO fromAddressToAddressDTO(Address address);

    @Mappings({@Mapping(source = "addressDTO.countryId", target = "country.id"),
               @Mapping(source = "addressDTO.stateId", target = "state.id")})
    Address fromAddressDTOToAddress(AddressDTO addressDTO);
}
