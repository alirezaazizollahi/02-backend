package ir.paizan.ecommerce.services.common;

import ir.paizan.ecommerce.services.dto.common.CountryDTO;

import java.util.List;

public interface CountryService {

    List<CountryDTO> findAll();
}
