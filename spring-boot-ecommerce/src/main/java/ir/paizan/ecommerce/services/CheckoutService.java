package ir.paizan.ecommerce.services;

import ir.paizan.ecommerce.services.dto.Purchase;
import ir.paizan.ecommerce.services.dto.PurchaseResponse;

public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);
}
