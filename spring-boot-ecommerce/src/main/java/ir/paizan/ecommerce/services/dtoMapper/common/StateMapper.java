package ir.paizan.ecommerce.services.dtoMapper.common;

import ir.paizan.ecommerce.entity.common.State;
import ir.paizan.ecommerce.services.dto.common.StateDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StateMapper {

    StateDTO fromStateToStateDTO(State state);

    List<StateDTO> fromStateToStateDTO(List<State> state);
}
