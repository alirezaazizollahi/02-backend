package ir.paizan.ecommerce.services.dtoMapper;

import ir.paizan.ecommerce.entity.Customer;
import ir.paizan.ecommerce.services.dto.CustomerDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerDTO fromCustomerToCustomerDTO(Customer customer);
    Customer fromCustomerDTOToCustomer(CustomerDTO customerDTO);
}
