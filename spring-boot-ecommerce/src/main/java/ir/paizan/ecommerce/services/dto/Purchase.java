package ir.paizan.ecommerce.services.dto;

import lombok.Data;

import java.util.Set;

@Data
public class Purchase {

    private CustomerDTO customer;
    private AddressDTO shippingAddress;
    private AddressDTO billingAddress;
    private OrderDTO order;
    private Set<OrderItemDTO> orderItems;
}
