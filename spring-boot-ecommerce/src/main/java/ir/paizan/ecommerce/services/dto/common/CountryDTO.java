package ir.paizan.ecommerce.services.dto.common;

import lombok.Data;

@Data
public class CountryDTO {

    private Long id;
    private String code;
    private String name;
}
