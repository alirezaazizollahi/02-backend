package ir.paizan.ecommerce.services.impl.common;

import ir.paizan.ecommerce.entity.common.State;
import ir.paizan.ecommerce.repository.common.StateRepository;
import ir.paizan.ecommerce.services.common.StateService;
import ir.paizan.ecommerce.services.dto.common.StateDTO;
import ir.paizan.ecommerce.services.dtoMapper.common.StateMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StateServiceImpl implements StateService{

    private StateRepository stateRepository;
    private StateMapper stateMapper;

    @Override
    public List<StateDTO> findAll() {
        List<State> stateList = stateRepository.findAll();
        return stateMapper.fromStateToStateDTO(stateList);
    }

    @Override
    public List<StateDTO> findByCountryCode(String code) {
        List<State> stateList = stateRepository.findByCountryCode(code);
        return stateMapper.fromStateToStateDTO(stateList);
    }

    @Override
    public List<StateDTO> findByCountryId(Long id) {
        List<State> stateList = stateRepository.findByCountryId(id);
        return stateMapper.fromStateToStateDTO(stateList);
    }
}
