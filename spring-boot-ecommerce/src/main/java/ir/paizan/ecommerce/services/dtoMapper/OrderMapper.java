package ir.paizan.ecommerce.services.dtoMapper;

import ir.paizan.ecommerce.entity.Order;
import ir.paizan.ecommerce.services.dto.OrderDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderDTO fromOrderToOrderDTO(Order order);
    Order fromOrderDTOToOrder(OrderDTO orderDTO);
}
