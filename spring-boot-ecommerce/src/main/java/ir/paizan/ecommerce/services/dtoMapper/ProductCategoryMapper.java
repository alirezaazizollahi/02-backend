package ir.paizan.ecommerce.services.dtoMapper;

import ir.paizan.ecommerce.entity.ProductCategory;
import ir.paizan.ecommerce.services.dto.ProductCategoryDTO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface ProductCategoryMapper {

    ProductCategoryDTO fromProductCategoryToProductCategoryDTO(ProductCategory productCategory);

    List<ProductCategoryDTO> fromProductCategoryToProductCategoryDTO(List<ProductCategory> productCategoryList);

}
