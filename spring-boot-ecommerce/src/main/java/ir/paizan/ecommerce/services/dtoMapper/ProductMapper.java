package ir.paizan.ecommerce.services.dtoMapper;


import ir.paizan.ecommerce.entity.Product;
import ir.paizan.ecommerce.services.dto.ProductDTO;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mappings({
            @Mapping(source = "product.category.id", target = "categoryId"),
            @Mapping(source = "product.category.categoryName", target = "categoryName")
    })
    ProductDTO fromProductToProductDTO(Product product);

    List<ProductDTO> fromProductToProductDTO(List<Product> productList);

}
