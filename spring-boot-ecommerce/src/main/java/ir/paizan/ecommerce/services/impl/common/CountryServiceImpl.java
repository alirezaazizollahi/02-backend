package ir.paizan.ecommerce.services.impl.common;

import ir.paizan.ecommerce.entity.common.Country;
import ir.paizan.ecommerce.repository.common.CountryRepository;
import ir.paizan.ecommerce.services.common.CountryService;
import ir.paizan.ecommerce.services.dto.common.CountryDTO;
import ir.paizan.ecommerce.services.dtoMapper.common.CountryMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CountryServiceImpl implements CountryService {

    private CountryRepository countryRepository;
    private CountryMapper countryMapper;

    @Override
    public List<CountryDTO> findAll() {
        List<Country> countryList = countryRepository.findAll();
        return countryMapper.fromCountryToCountryDTO(countryList);
    }
}
