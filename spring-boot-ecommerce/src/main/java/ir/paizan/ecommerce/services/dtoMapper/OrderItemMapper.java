package ir.paizan.ecommerce.services.dtoMapper;

import ir.paizan.ecommerce.entity.OrderItem;
import ir.paizan.ecommerce.services.dto.OrderItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface OrderItemMapper {

    OrderItemDTO fromOrderItemToOrderItemDTO(OrderItem orderItem);
    @Mapping(source = "orderItemDTO.productId", target = "product.id")
    OrderItem fromOrderItemDTOToOrderItem(OrderItemDTO orderItemDTO);

    Set<OrderItemDTO> fromOrderItemToOrderItemDTO(Set<OrderItem> orderItem);
    Set<OrderItem> fromOrderItemDTOToOrderItem(Set<OrderItemDTO> orderItemDTOS);
}
