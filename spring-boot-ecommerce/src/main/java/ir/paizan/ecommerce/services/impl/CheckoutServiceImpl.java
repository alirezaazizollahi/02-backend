package ir.paizan.ecommerce.services.impl;

import ir.paizan.ecommerce.entity.Customer;
import ir.paizan.ecommerce.entity.Order;
import ir.paizan.ecommerce.entity.OrderItem;
import ir.paizan.ecommerce.repository.CustomerRepository;
import ir.paizan.ecommerce.services.CheckoutService;
import ir.paizan.ecommerce.services.dto.Purchase;
import ir.paizan.ecommerce.services.dto.PurchaseResponse;
import ir.paizan.ecommerce.services.dtoMapper.AddressMapper;
import ir.paizan.ecommerce.services.dtoMapper.CustomerMapper;
import ir.paizan.ecommerce.services.dtoMapper.OrderItemMapper;
import ir.paizan.ecommerce.services.dtoMapper.OrderMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;
import java.util.UUID;

@Service
@AllArgsConstructor
public class CheckoutServiceImpl implements CheckoutService {

    private CustomerRepository customerRepository;
    private CustomerMapper customerMapper;
    private AddressMapper addressMapper;
    private OrderMapper orderMapper;
    private OrderItemMapper orderItemMapper;

    @Override
    @Transactional
    public PurchaseResponse placeOrder(Purchase purchase) {


        // retrieve the order info from dto
        Order order = orderMapper.fromOrderDTOToOrder(purchase.getOrder());

        // generate tracking number
        String orderTrackingNumber = generateOrderTrackingNumber();
        order.setOrderTrackingNumber(orderTrackingNumber);

        // populate order with order items
        Set<OrderItem> orderItems = orderItemMapper.fromOrderItemDTOToOrderItem(purchase.getOrderItems());
        order.addAll(orderItems);

        // populate order with billing address and shipping address
        order.setBillingAddress(addressMapper.fromAddressDTOToAddress(purchase.getBillingAddress()));
        order.setShippingAddress(addressMapper.fromAddressDTOToAddress(purchase.getShippingAddress()));

        // populate customer with order
        Customer customer = customerMapper.fromCustomerDTOToCustomer(purchase.getCustomer());
        customer.add(order);

        // save to the database
        customerRepository.save(customer);

        // return a response
        return new PurchaseResponse(orderTrackingNumber);
    }

    private String generateOrderTrackingNumber() {

        // generate a random UUID number (UUID version-4)
        return UUID.randomUUID().toString();

    }
}
