package ir.paizan.ecommerce.services.dto;

import lombok.Data;

@Data
public class AddressDTO {

    private Long id;
    private String city;
    private Long countryId;
    private String street;
    private String zipCode;
    private Long stateId;
    private OrderDTO order;
}
