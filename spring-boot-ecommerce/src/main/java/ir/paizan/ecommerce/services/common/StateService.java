package ir.paizan.ecommerce.services.common;

import ir.paizan.ecommerce.services.dto.common.StateDTO;

import java.util.List;

public interface StateService {

    List<StateDTO> findAll();

    List<StateDTO> findByCountryCode(String code);

    List<StateDTO> findByCountryId(Long id);

}
