package ir.paizan.ecommerce.services.dtoMapper.common;

import ir.paizan.ecommerce.entity.common.Country;
import ir.paizan.ecommerce.services.dto.common.CountryDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CountryMapper {

    CountryDTO fromCountryToCountryDTO(Country country);
    List<CountryDTO> fromCountryToCountryDTO(List<Country> countries);

}
