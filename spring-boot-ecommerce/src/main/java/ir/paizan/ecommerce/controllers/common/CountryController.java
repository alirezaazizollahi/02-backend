package ir.paizan.ecommerce.controllers.common;

import ir.paizan.ecommerce.services.common.CountryService;
import ir.paizan.ecommerce.services.dto.common.CountryDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin("http://localhost:4200")
public class CountryController {

    private CountryService countryService;

    @GetMapping("/countries")
    public List<CountryDTO> getCountries(){
        return countryService.findAll();
    }
}
