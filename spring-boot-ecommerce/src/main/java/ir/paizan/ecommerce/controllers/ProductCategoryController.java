package ir.paizan.ecommerce.controllers;

import ir.paizan.ecommerce.entity.ProductCategory;
import ir.paizan.ecommerce.services.ProductCategoryService;
import ir.paizan.ecommerce.services.dtoMapper.ProductCategoryMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin("http://localhost:4200")
public class ProductCategoryController {

    private ProductCategoryService productCategoryService;
    private ProductCategoryMapper productCategoryMapper;

    @GetMapping("/product-category")
    public ResponseEntity<List<ProductCategory>> getProductCategories(){
         List<ProductCategory> productCategoryList = productCategoryService.findAll();
         return new ResponseEntity(productCategoryMapper.fromProductCategoryToProductCategoryDTO(productCategoryList), HttpStatus.OK);
    }
}
