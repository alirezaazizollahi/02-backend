package ir.paizan.ecommerce.controllers;


import ir.paizan.ecommerce.services.CheckoutService;
import ir.paizan.ecommerce.services.dto.Purchase;
import ir.paizan.ecommerce.services.dto.PurchaseResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/checkout")
@CrossOrigin("http://localhost:4200")
public class CheckoutController {

    private CheckoutService checkoutService;

    @PostMapping("/purchase")
    public PurchaseResponse placeOrder(@RequestBody Purchase purchase){
        PurchaseResponse response = checkoutService.placeOrder(purchase);
        return response;
    }

}
