package ir.paizan.ecommerce.controllers;

import ir.paizan.ecommerce.entity.Product;
import ir.paizan.ecommerce.services.ProductService;
import ir.paizan.ecommerce.services.dto.ProductDTO;
import ir.paizan.ecommerce.services.dtoMapper.ProductMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin("http://localhost:4200")
public class ProductController {

    private ProductService productService;
    private ProductMapper productMapper;

    @GetMapping("/products")
    public ResponseEntity<Page<ProductDTO>> getProducts(Pageable pageable){
        Page<ProductDTO> page = productService.findAll(pageable);
        return new ResponseEntity(page, HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable(required = true) Long id){
        Product product = productService.findById(id);
        return new ResponseEntity(productMapper.fromProductToProductDTO(product), HttpStatus.OK);
    }
    @GetMapping("/products/findByCategoryId")
    public ResponseEntity<Page<ProductDTO>> getByCategoryId(@RequestParam(name = "categoryId") Long categoryId, Pageable pageable){
        Page<ProductDTO> page = productService.findByCategoryId(categoryId, pageable);
        return new ResponseEntity(page, HttpStatus.OK);
    }

    @GetMapping("/products/findByName")
    public ResponseEntity<List<ProductDTO>> getProductsByName(@RequestParam(name = "name") String name, Pageable pageable){
        Page<ProductDTO> page = productService.findByName(name, pageable);
        return new ResponseEntity(page, HttpStatus.OK);
    }
}
