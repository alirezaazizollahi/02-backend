package ir.paizan.ecommerce.controllers.common;

import ir.paizan.ecommerce.services.common.StateService;
import ir.paizan.ecommerce.services.dto.common.StateDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin("http://localhost:4200")
public class StateController {

    private StateService stateService;

    @GetMapping("/states")
    public List<StateDTO> getStates(){
        return stateService.findAll();
    }

    @GetMapping("/states/findByCountryCode")
    public List<StateDTO> getByCountryCode(@RequestParam String code){
        return stateService.findByCountryCode(code);
    }

    @GetMapping("/states/findByCountryId")
    public List<StateDTO> getByCountryId(@RequestParam Long id){
        return stateService.findByCountryId(id);
    }


}
