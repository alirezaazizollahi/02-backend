package ir.paizan.ecommerce.repository.common;

import ir.paizan.ecommerce.entity.common.Country;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CountryRepository extends JpaRepository<Country, Long> {
}
