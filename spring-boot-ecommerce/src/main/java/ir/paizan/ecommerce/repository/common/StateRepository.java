package ir.paizan.ecommerce.repository.common;

import ir.paizan.ecommerce.entity.common.State;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StateRepository extends JpaRepository<State, Long> {

    List<State> findByCountryCode(String code);

    List<State> findByCountryId(Long id);
}
